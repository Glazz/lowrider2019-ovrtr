#pragma once

#include "AHRS.h"
#include "frc/SPI.h"
class VaporGyro
{
public:
    VaporGyro();
    double getYaw();
    void Reset();
    double getRealYaw();
    float getAccelX();
    float getAccelY();
    float getAccelZ();
    float getAlt();
    bool magNoise();
    bool isAutoCal();
    float getRoll();

private:
    //frc::SPI::Port kMXP;
    frc::SPI::Port kGyroPort;
    AHRS navx;
};
