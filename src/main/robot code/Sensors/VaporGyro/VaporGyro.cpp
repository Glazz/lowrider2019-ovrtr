#include "Sensors/VaporGyro/VaporGyro.h"
VaporGyro::VaporGyro() : navx(frc::SPI::Port::kMXP) {
    navx.Reset();
}
double VaporGyro::getYaw() {
    double angle = navx.GetAngle();
    if (angle < 0.0) {
        while (angle < 360.0) {
            angle = angle + 360.0;
        }
    }
    if (angle > 0.0) {
        while (angle > 360.0) {
            angle = angle - 360.0;
        }
    }
    return angle;
}

void VaporGyro::Reset() {
    navx.Reset();
    navx.ResetDisplacement();
}

double VaporGyro::getRealYaw() {
    return navx.GetRoll();
}

float VaporGyro::getAccelX() {
    return navx.GetWorldLinearAccelX();
}
float VaporGyro::getAccelY() {
    return navx.GetWorldLinearAccelY();
}
float VaporGyro::getAccelZ() {
    return navx.GetWorldLinearAccelZ();
}
bool VaporGyro::magNoise() {
    return navx.IsMagneticDisturbance();
}

bool VaporGyro::isAutoCal() {
    return navx.IsCalibrating();
}
float VaporGyro::getAlt() {
    return navx.GetAltitude();
}
float VaporGyro::getRoll() {
    return navx.GetRoll();
}