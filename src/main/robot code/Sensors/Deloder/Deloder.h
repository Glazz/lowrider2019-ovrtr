#pragma once
#include "frc/Encoder.h"

class Deloder {
public:
    Deloder(unsigned int mxp1, unsigned int mxp2);
    double GetDistancePerPulse();
    double GetDistance();
    void Reset();
    int GetRaw();
    void SetDistance(unsigned int pulses);
private:
    frc::Encoder m_encoder;
};