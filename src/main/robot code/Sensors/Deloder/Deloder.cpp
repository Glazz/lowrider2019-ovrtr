#include "Sensors/Deloder/Deloder.h"


Deloder::Deloder(unsigned int mxp1, unsigned int mxp2) : m_encoder(mxp1, mxp2,true) {
    m_encoder.SetDistancePerPulse(4096);
}

void Deloder::Reset() {
    m_encoder.Reset();
}

double Deloder::GetDistance(){
    m_encoder.GetDistance();
}
double Deloder::GetDistancePerPulse(){
    m_encoder.GetRaw();
}

void Deloder::SetDistance(unsigned int pulses) {
   m_encoder.SetDistancePerPulse(pulses);
 }

int Deloder::GetRaw() {
    return m_encoder.Get();
}

