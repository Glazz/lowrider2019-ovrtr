#include "Robot.h"
void Robot::RobotInit() {
   // std::thread Odometry_thread(&Neodometry::OdometryThread, &m_delorean_encs, std::ref(m_yaw));
   // std::thread Pike_thread(&Disker::PikeThread, &m_disker, std::ref(m_pike_thread_trigger));
    //test = std::thread(&Chassis::TryingThreads, &chassis);
    
    if (m_camera_switch) {
        #if defined(__linux__)
            frc::CameraServer::GetInstance()->StartAutomaticCapture(0);

        #else
            wpi::errs() << "Vision only available on Linux.\n";
            wpi::errs().flush();
        #endif
    }
   frc::SmartDashboard::PutNumber("Target Y", 0.0);
   frc::SmartDashboard::PutNumber("Target X", 0.0);
}
void Robot::RobotPeriodic() {
     yaw.store(navx.getRealYaw());
     frc::SmartDashboard::PutNumber("Angle", navx.getRealYaw());
     frc::SmartDashboard::PutNumber("Left joystick", m_mando.GetY(frc::XboxController::kLeftHand));
     frc::SmartDashboard::PutNumber("Right Joystick", m_mando.GetY(frc::XboxController::kRightHand));
     std::cout << m_delorean_encs.GetLeft() << " " << m_delorean_encs.GetRight() << std::endl;
     frc::SmartDashboard::PutNumber("X coord", m_delorean_encs.GetCoords().x);
     frc::SmartDashboard::PutNumber("Y coord", m_delorean_encs.GetCoords().y);
}

void Robot::AutonomousInit() {
    ytarget = frc::SmartDashboard::GetNumber("Target Y", 0.0);
    xtarget = frc::SmartDashboard::GetNumber("Target X", 0.0);
}
void Robot::AutonomousPeriodic() {
        
    double target_ang = std::atan2(ytarget - m_delorean_encs.GetCoords().y, xtarget - m_delorean_encs.GetCoords().x);
    if (std::sqrt(std::pow(xtarget - m_delorean_encs.GetCoords().x,2) + std::pow(ytarget-m_delorean_encs.GetCoords().y,2)) > .30){
         chassis.StraightDrive(target_ang, yaw.load());
         std::cout << "YAW: " << yaw.load() << endl;
    } else {
        chassis.Brake();
    }
}

void Robot::TeleopInit() {
        robot_enabled.store(true);

     std::thread Speed_thread(&Neodometry::NeodometryThread,
                             &m_delorean_encs,
                             std::ref(robot_enabled),
                             std::ref(yaw));
     Speed_thread.detach();
   
   // m_piston.Off();
    navx.Reset();
}

void Robot::TeleopPeriodic() {

    double l_out = limiter.Filter(-m_mando.GetY(frc::XboxController::kLeftHand));
    double r_out = limiter.Filter(-m_mando.GetY(frc::XboxController::kRightHand));

    //Limiter Enabler
    if (m_mando.GetStartButton()) {
        limiter.SwitchLimit();
        if (limiter.ReturnState()) {
            frc::SmartDashboard::PutBoolean("Limiter", true);
        }
        else {
            frc::SmartDashboard::PutBoolean("Limiter", false);
        }
    }

    //ROTATION BY BUMPER-----------------------------------
    if (m_mando.GetBumper(frc::XboxController::kRightHand)) {
        use_joys = false;
        frc::SmartDashboard::PutBoolean("Right Bumper", true);
        l_out -= m_rotation_speed;
        r_out += m_rotation_speed;
        chassis.Drive(m_rotation_speed, m_rotation_speed);
    }
    else {
        use_joys = true;
        frc::SmartDashboard::PutBoolean("Right Bumper", false);
    }

    if (m_mando.GetBumper(frc::XboxController::kLeftHand)) {
        use_joys = false;
        frc::SmartDashboard::PutBoolean("Left Bumper", true);
        l_out += m_rotation_speed;
        r_out -= m_rotation_speed;
        chassis.Drive(m_rotation_speed, -m_rotation_speed);
    }
    else {
        use_joys = true;
        frc::SmartDashboard::PutBoolean("Left Bumper", false);
    }
    //-----------------------------------------------------------------


    //WIP STR8 DRIVE----------------------------------------------------
   

    if (m_mando.GetStickButtonPressed(frc::XboxController::kRightHand)) {
        chassis.Drive(0.5,0.5);
    }
    
    if (m_mando.GetRawButtonPressed(2)) {
        neorams.FrontControl();
    }  else if (m_mando.GetRawButtonReleased(2)) {
        neorams.FrontContorlOff();
    }

    //-----------------------------------------------------------------
    if (m_mando.GetRawButton(3)) { //XBUTTON
        neorams.BackControl();
    } else if (m_mando.GetRawButtonReleased(3)) {
        neorams.BackControlOff();
    }
   
    //---------------------------------------------------------------
   
    //Inverter
    if (m_mando.GetStickButton(frc::XboxController::kLeftHand)) {
        chassis.InvertFront();
    }

    if (m_mando.GetTriggerAxis(frc::XboxController::kLeftHand)) {
        chassis.GetRewindCommands(m_delorean_encs.Rewind());
    }

    //STRAIGHT DRIVE
    if (m_mando.GetAButtonPressed()) {
        use_joys = false;
        m_fixed_angle = yaw;
    } if (m_mando.GetAButton()) {
        chassis.StraightDrive(m_fixed_angle, yaw);
    } else if (m_mando.GetAButtonReleased()) {
        use_joys = true;
    }

    if (m_mando.GetXButtonPressed()) {
        use_joys = false;
        m_delorean_encs.RecordSwitch(false);
    }
    if (m_mando.GetXButton()) {
        chassis.GetRewindCommands(m_delorean_encs.Rewind());
    } 

    if (m_mando.GetXButtonReleased()) {
        chassis.Brake();
        m_delorean_encs.RecordSwitch(true);
    }
    

    // PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2-PLAYER2
     if (std::abs(player2.GetY(frc::Joystick::kRightHand)) > 0.05) {
        m_disker.Elevator(player2.GetY(frc::Joystick::kRightHand));
    } else { 
        m_disker.Elevator(0.0);
    }
    
    if (player2.GetAButtonPressed()) { //A BUTTON
            m_disker.PushOrPull();
        //  frc::SmartDashboard::PutBoolean("Piston2", m_piston2.ReturnSet());
        } else if (player2.GetAButtonReleased()) {
            m_disker.PistonsOff();
        }

    if (player2.GetYButton()) {
        m_disker.PikeControl(true);
    } else if (player2.GetYButtonReleased()){
        m_disker.PikeBrake();
    }
    if (player2.GetXButton()) {
         m_disker.PikeControl(false);
    } else if (player2.GetXButtonReleased()){
        m_disker.PikeBrake();
    }

    if (use_joys) {
        chassis.Drive(l_out, r_out);
    }

}
void Robot::TestInit() {
}
void Robot::TestPeriodic() {
}

#ifndef RUNNING_FRC_TESTS
int main() {
    return frc::StartRobot<Robot>();
}
#endif