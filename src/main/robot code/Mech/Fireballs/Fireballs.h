#pragma once
#include "frc/Encoder.h"
#include "Utilities/NeonVictorSPX.h"

class Fireballs {

public:
    Fireballs(unsigned int vic_port1,unsigned int succ_port, unsigned int dio1, unsigned int dio2);
    void RotateDown();
    void RotateUp();
    void PreGameStatus();
    int GetPulse();
    void SuccBall();
    void ShootBall();
    void BrakeSucc();
    double GetDistance();
    
    //void Rotate(float target);
private:
    frc::Encoder m_encoder;
    NeonVictorSPX arm_vic;
    NeonVictorSPX succ_vic;
    
};