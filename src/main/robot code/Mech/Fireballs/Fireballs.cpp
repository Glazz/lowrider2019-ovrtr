#include "Mech/Fireballs/Fireballs.h"

Fireballs::Fireballs(unsigned int vic_port1,unsigned int succ_port, unsigned int mxp1, unsigned int mxp2) :
    arm_vic(vic_port1),succ_vic(succ_port), m_encoder(mxp1,mxp2,true,frc::Encoder::k4X) {
    m_encoder.SetDistancePerPulse(0.00015640273);
    succ_vic.ConfigOpenloopRamp(1/6);
    succ_vic.SetNeutralMode(motorcontrol::Coast);
}

//void Fireballs::Rotate()

void Fireballs::RotateDown() {;
    if (m_encoder.GetDistance() < 0.25) {
        arm_vic.Set(ControlMode::PercentOutput, -0.3);
    } else {
        arm_vic.Set(ControlMode::PercentOutput, 0.0);
    }
}

void Fireballs::RotateUp() {
    if (m_encoder.GetDistance() > 0.0) {
        arm_vic.Set(ControlMode::PercentOutput, 0.3);
    } else {
        arm_vic.Set(ControlMode::PercentOutput, 0.0);
    }
}

void Fireballs::SuccBall() {
    succ_vic.Power(0.4);
}

void Fireballs::ShootBall() {
    succ_vic.Power(-0.6);
}

void Fireballs::BrakeSucc() {
    succ_vic.Brake();
}
void Fireballs::PreGameStatus() {
    if (m_encoder.GetDistance() < -0.12) {
        arm_vic.Set(ControlMode::PercentOutput, 0.3);
    } else {
        arm_vic.Set(ControlMode::PercentOutput, 0.0);
    }
}


int Fireballs::GetPulse() {
    return m_encoder.Get();
}

double Fireballs::GetDistance() {
    return m_encoder.GetDistance();
}
