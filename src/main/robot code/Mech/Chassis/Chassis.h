#pragma once
#include "Utilities/Limiter/Limiter.h"
#include <iostream>
#include "Utilities/NeonVictorSPX.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include "frc/XboxController.h"
#include "Utilities/Datastructs/MotorOutputs.h"
#include <atomic>
#include <vector>
using namespace std;


class Chassis {

public:
  Chassis(unsigned int r_1, unsigned intr_2, unsigned int l_1, unsigned int l_2);
  void Drive(float left, float right);
  void StraightDrive(double target_angle, double current_angle);
  void Brake();
  void SeekLego(int x);
  void InvertFront();
  void GetRewindCommands(MotorOutputs input);
  
private:
  NeonVictorSPX right_master_spx;
  NeonVictorSPX right_slave;
  NeonVictorSPX left_master_spx;
  NeonVictorSPX left_slave;
  std::atomic<double> left_command;
  std::atomic<double> right_command;
  double limit = 0.5;
  bool limit_state = true;
  


};