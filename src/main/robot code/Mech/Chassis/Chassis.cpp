#include "Chassis.h"

//MOTORS SETUP
Chassis::Chassis(unsigned int r_1, unsigned int r_2, unsigned int l_1, unsigned int l_2) : 
right_master_spx(r_1), right_slave(r_2), left_master_spx(l_1), left_slave(l_2){
    

    right_slave.Set(ControlMode::Follower, right_master_spx.GetDeviceID());
    left_slave.Set(ControlMode::Follower, left_master_spx.GetDeviceID());
    right_slave.SetInverted(true);
    right_master_spx.SetInverted(true);
}

void Chassis::Drive(float left_output, float right_output) {
    if (std::abs(left_output) < 0.05) {
        left_output = 0.0;
    }
    if (std::abs(right_output) < 0.05) {
        right_output = 0.0;
    }
    left_master_spx.Power(left_output);
    right_master_spx.Power(right_output);
   /* left_master_spx.Set(ControlMode::PercentOutput, left_output);
    right_master_spx.Set(ControlMode::PercentOutput, right_output);*/
    frc::SmartDashboard::PutNumber("Left motor", left_master_spx.GetMotorOutputPercent());
    frc::SmartDashboard::PutNumber("Right motor", right_master_spx.GetMotorOutputPercent());
}


void Chassis::StraightDrive(double target_angle, double current_ang) {
    double speed = 0.4;
    double error = target_angle - current_ang;
    double output = error * 0.021;
    Drive(speed - output, speed + output);
}


void Chassis::Brake() {
    Drive(0.0,0.0);
}
void Chassis::SeekLego(int x) {
    double rx = 640.0;
    double speed = 0.7;
    double center = rx  / 2;
    float error = center - x;
    if (std::abs(error) > 20) {
        double output  = error / rx;
        output = output / 2;
        Drive(output,-output);
    }
    else {
        Brake();
    }
}
void Chassis::InvertFront() {
    if (right_master_spx.GetInverted()) {
        right_master_spx.SetInverted(false);
        left_master_spx.SetInverted(true);
    } else {
        right_master_spx.SetInverted(true);
        left_master_spx.SetInverted(false);
    }
}

void Chassis::GetRewindCommands(MotorOutputs input) {
    Drive(input.left, input.right);
}