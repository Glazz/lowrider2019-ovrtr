#include "Mech/Solenoid/Base/VaporDoubleSolenoid.h"

class DDPiston {
public:
    DDPiston(unsigned int port1, unsigned int port2, unsigned int port3, unsigned int port4);
    void Forward();
    void Reverse();
    void Off();
private:
    VaporDoubleSolenoid m_1;
    VaporDoubleSolenoid m_2;
};