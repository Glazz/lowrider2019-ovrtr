#include "Mech/Solenoid/PairOfSuccers/DDPiston.h"

DDPiston::DDPiston(unsigned int port1, unsigned int port2, unsigned int port3,unsigned int port4) :
    m_1(port1, port2), m_2(port3, port4) {
}

void DDPiston::Forward() {
    m_1.Forward();
    m_2.Forward();
}

void DDPiston::Reverse() {
    m_1.Reverse();
    m_2.Reverse();
}

void DDPiston::Off() {
    m_1.Off();
    m_2.Off();
}