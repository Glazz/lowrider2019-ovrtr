#pragma once
#include "frc/DoubleSolenoid.h"


class VaporDoubleSolenoid {

public:
    VaporDoubleSolenoid(unsigned int port0, unsigned int port1);
    void Forward();
    void Off();
    void Reverse();
    void PushOrPull();
    bool ReturnSet();
private:
    bool set = true;
    frc::DoubleSolenoid m_doubleSolenoid;
};