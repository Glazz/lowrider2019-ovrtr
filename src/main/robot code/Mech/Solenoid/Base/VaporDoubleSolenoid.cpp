#include "VaporDoubleSolenoid.h"
VaporDoubleSolenoid::VaporDoubleSolenoid(unsigned int port0, unsigned int port1):
    m_doubleSolenoid(port0, port1) {
}

void VaporDoubleSolenoid::Forward() {
    m_doubleSolenoid.Set(frc::DoubleSolenoid::kForward);
}

void VaporDoubleSolenoid::Off() {
    m_doubleSolenoid.Set(frc::DoubleSolenoid::kOff);
}

void VaporDoubleSolenoid::Reverse() {
    m_doubleSolenoid.Set(frc::DoubleSolenoid::kReverse);
}

void VaporDoubleSolenoid::PushOrPull() {
    if (set) {  //forward
        set = false; 
        m_doubleSolenoid.Set(frc::DoubleSolenoid::kForward);
    } else {    //not forward
        set = true; 
        m_doubleSolenoid.Set(frc::DoubleSolenoid::kReverse);
    }
}
bool VaporDoubleSolenoid::ReturnSet() {
    return set;
}



