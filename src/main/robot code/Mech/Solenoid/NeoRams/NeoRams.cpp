#include "Mech/Solenoid/NeoRams/NeoRams.h"

NeoRams::NeoRams(unsigned int f1, unsigned int f2,
unsigned int b1, unsigned int b2) : fronts(f1,f2), backs(b1,b2) {
}

void NeoRams::FrontControl() {
    fronts.PushOrPull();
}

void NeoRams::FrontContorlOff() {
    fronts.Off();
}

void NeoRams::BackControl() {
    backs.PushOrPull();
}

void NeoRams::BackControlOff() {
    backs.Off();
}