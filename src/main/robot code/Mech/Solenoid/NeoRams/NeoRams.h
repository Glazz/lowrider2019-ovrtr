#pragma once
#include "Mech/Solenoid/Base/VaporDoubleSolenoid.h"

class NeoRams
{
  public:
    NeoRams(unsigned int f1, unsigned int f2, unsigned int b1, unsigned int b2);
    void FrontControl();
    void FrontContorlOff();
    void BackControl();
    void BackControlOff();
  private:
    VaporDoubleSolenoid fronts;
    VaporDoubleSolenoid backs;
};