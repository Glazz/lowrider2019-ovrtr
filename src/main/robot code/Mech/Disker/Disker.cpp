#include "Mech/Disker/Disker.h"

Disker::Disker(unsigned int one, unsigned int two,
               unsigned int pikeport, unsigned int elv1port,
               unsigned int elv2port,
               unsigned int pikenc1, unsigned int pikenc2, 
               unsigned int elevcoder1,unsigned int elevcoder2):

               m_pistons(one, two), pike(pikeport),
               elev1(elv1port), elev2(elv2port),elevcoder(elevcoder1, elevcoder2), pike_enc(pikenc1, pikenc2, false,frc::Encoder::k4X) 
                {
        
        elev2.Set(ControlMode::Follower,elev1.GetDeviceID());
        elev2.SetInverted(true);
}

void Disker::PikeAction() {
    action = !action;
    if (action) {
        if (is_pike_open) { //we close it
            if (pike_enc.GetDistance() > 0.09) {
                pike.Power(-1.0); //assumming -1 is the right value lol
            }
            else if (pike_enc.GetDistance() < 0.09) {
                pike.Brake();
                action = false;
                is_pike_open = false;
            }
        } else if (!is_pike_open) { //pike starts closed so we open it 
            if (pike_enc.GetDistance() < 0.09) {
                pike.Power(1.0);
            }
            pike.Brake();
            is_pike_open = true;
            action = false;
        }
    }
}

void Disker::Shoot() {
        m_pistons.Forward();
}

void Disker::Reverse() {
        m_pistons.Reverse();
    }

void Disker::PistonsOff() {
    m_pistons.Off();
}

void Disker::PushOrPull() {
    m_pistons.PushOrPull();
}

void Disker::Elevator(float speed) {
    elev1.Power(speed / 2);
}

void Disker::PikeControl(bool  direction) {
   if (direction) {
       pike.Power(1.0);
   } else if (!direction) {
       pike.Power(-1.0);
   }
}

float Disker::Pget(float target, float now) {
    float error = target - now;
    float correction = error * 0.021 / 2;
    return correction;
}

void Disker::SecondLevel() { // to be refactored, must ressemble Disker's PikeAction
    elev_set = !elev_set;
    if (elev_set) {
        elev1.Power( Pget(ELEVATOR_HEIGHT, elevcoder.GetDistance()) );
    }
    else {
        elev1.Power( Pget(0.0, elevcoder.GetDistance()) );
    }
}

void Disker::PikeBrake() {
    pike.Brake();
}

double Disker::GetPulses() {
    return elevcoder.Get();
}


