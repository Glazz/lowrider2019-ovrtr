#pragma once
#include "Mech/Solenoid/Base/VaporDoubleSolenoid.h"
//#include "Mech/Solenoid/PairOfSuccers/DDPiston.h"
#include "Utilities/NeonVictorSPX.h"
#include "frc/DigitalInput.h"
#include <chrono>
#include "frc/Encoder.h"
#include "Sensors/Deloder/Deloder.h"

class Disker {
public:
    Disker(unsigned int one, unsigned int two, unsigned int pikeport, unsigned int elv1port, 
           unsigned int elv2port,unsigned int elevcoder1, unsigned int elevcoder2, 
           unsigned int pikenc1, unsigned int pikenc2);
    void Reverse();
    void Shoot();
    void PistonsOff();
    void PikeAction();
    void ShootingSequence(bool done_yet);
    void SecondLevel();
    double GetPulses();
    void PushOrPull();
    void Elevator(float speed);
    void PikeControl(bool direction);
    void PikeBrake();
private:
    
    /*VaporDoubleSolenoid piston1;
    VaporDoubleSolenoid piston2;*/
    VaporDoubleSolenoid m_pistons;
    NeonVictorSPX pike;
    frc::Encoder pike_enc;
    frc::Encoder elevcoder;
    NeonVictorSPX elev1;
    NeonVictorSPX elev2;
    bool action = false;
    float Pget(float error, float now);
    const float ELEVATOR_HEIGHT = -0.74; //cm
    bool elev_set = false;
    void Push();
    void Succ();
    bool is_pike_open = true;
    bool done_yet = true; 
};