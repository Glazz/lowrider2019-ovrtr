#pragma once
#include "Mech/Chassis/Chassis.h"
#include "Mech/Disker/Disker.h"
#include "Mech/Fireballs/Fireballs.h"
#include "Mech/Solenoid/Base/VaporDoubleSolenoid.h"
#include "Mech/Solenoid/NeoRams/NeoRams.h"
#include "Sensors/VaporGyro/VaporGyro.h"
#include "Utilities/Neodometry/Neodometry.h"
#include "Utilities/NeonVictorSPX.h"
#include "Mech/Solenoid/NeoRams/NeoRams.h"
#include "Utilities/Datastructs/RecordFrame.h"
#include "frc/Joystick.h"
#include "frc/Timer.h"
#include <cameraserver/CameraServer.h>
#include <frc/Spark.h>
#include <frc/TimedRobot.h>
#include <frc/WPILib.h>
#include <frc/XboxController.h>
#include <frc/smartdashboard/SmartDashboard.h>
#include <iostream>
#include <thread>
#include <wpi/raw_ostream.h>



using namespace std;

class Robot : public frc::TimedRobot {
public:
    void RobotInit() override;
    void RobotPeriodic() override;
    void AutonomousInit() override;
    void AutonomousPeriodic() override;
    void TeleopInit() override;
    void TeleopPeriodic() override;
    void TestPeriodic() override;
    void TestInit() override;
private:
    std::thread test;
    VaporGyro navx;
    Limiter limiter;
    frc::XboxController m_mando{0};
    Chassis chassis{9,7,4,5}; //CAN_IDS FOR VICTORSPX'S
    //Fireballs fireballer{3,1, /*encoder ports*/6,7};
    std::atomic<double> yaw;
    std::atomic<bool> robot_enabled;
    Disker m_disker {
        /*Pistons*/ 6,7,
       /*pike vic*/ 3,
        /*elevator motors*/ 2,8,
        /*pike encoder ports*/8,9,
        /*elevator encoder ports */ 0,1};
    NeoRams neorams{0,1,4,5};
    Neodometry m_delorean_encs{2,3,4,5};
    frc::XboxController player2{1};
    bool m_pike_thread_trigger = false;
    double m_fixed_angle = 0.0;
    double max_accel = 2.0;
    double max_speed = 1.0;
    double desired_out = 0.0;
    double current_out = 0.0;
    bool use_joys = true;
    int started = 0;
    const double m_rotation_speed = 0.4;
    bool m_camera_switch = false;
    bool m_recording = true; 
    double ytarget = 0.0;
    double xtarget = 0.0;
};
