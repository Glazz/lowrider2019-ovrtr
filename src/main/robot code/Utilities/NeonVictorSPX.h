#pragma once
#include "ctre/Phoenix.h"

class NeonVictorSPX : public VictorSPX {
public:
    NeonVictorSPX(unsigned int can_id);
    void Power(float input);
    void DisableRamp();
    void OverWriteRamp(float set);
    void Brake();
};