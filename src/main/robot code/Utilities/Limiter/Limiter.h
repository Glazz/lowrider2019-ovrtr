#pragma once
#include <cmath>
class Limiter{  
    public:
        Limiter();
        void SwitchLimit();
        bool ReturnState();
        double ReturnLimit();
        double Filter(double input);
       
    private:
        const double limit = 0.68;
        bool limit_state = false;
        
};