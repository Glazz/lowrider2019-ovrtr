#include "Limiter.h"

Limiter::Limiter(){

}
void Limiter::SwitchLimit(){
    if (limit_state){
        limit_state = false;
    }
    else{
        limit_state = true;
    }
}
double Limiter::ReturnLimit(){
    return limit;
}
bool Limiter::ReturnState(){
    return limit_state;
}

double Limiter::Filter(double input){
    if (std::abs(input) < 0.05){
        input = 0.0;
    }
    if (limit_state){
        if (input > limit){
            input = limit;
        }
        if (input < -limit){
            input = -limit;
        }
    }

      return input;
}
