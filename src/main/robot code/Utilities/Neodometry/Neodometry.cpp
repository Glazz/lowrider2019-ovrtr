#include "Utilities/Neodometry/Neodometry.h"

Neodometry::Neodometry(unsigned int dio1,unsigned int dio2,unsigned int dio3,unsigned int dio4):
    m_left_encoder(dio1, dio2,true), m_right_encoder(dio3, dio4){
        //m_left_encoder.SetDistancePerPulse(0.00046511627);
        m_left_encoder.SetDistancePerPulse(0.000116889);
        //radius of 46 cm aprox
        //m_right_encoder.SetDistancePerPulse(0.00046511627);
        m_right_encoder.SetDistancePerPulse(0.000116889); //thats pulse per meter
        x.store(0.0);
        y.store(0.0);
        m_recording.store(true);
}


void Neodometry::NeodometryThread(std::atomic<bool>& flag,std::atomic<double>& yaw) {
    auto time_start = std::chrono::high_resolution_clock::now();
    while(flag.load()) {
            
            double current_yaw = yaw * M_PI / 180;
            double left_start = m_left_encoder.GetDistance();
            double right_start = m_right_encoder.GetDistance();
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
            auto frame_time = std::chrono::high_resolution_clock::now();
            double time_secs = std::chrono::duration<double>(frame_time - time_start).count();
            //ZZZZZZZZzzzzzZZZZZ
            left_speed = m_left_encoder.GetDistance() - left_start;
            right_speed = m_right_encoder.GetDistance() - right_start;
            
            m_speed = (left_speed + right_speed) / 2;
            yspeed_real = m_speed * std::sin(current_yaw);
            xspeed_real = m_speed * std::cos(current_yaw); 
            double new_x = x.load() + xspeed_real;
            double new_y = y.load() + yspeed_real;
            x.store(new_x);
            y.store(new_y);
            
            //REWIND CODE
            if (m_recording.load()) {
                Frame.left_distance = left_speed;
                Frame.right_distance = right_speed;
                Frame.timestamp = time_secs;
                Trace.push_back(Frame);
                if (Trace.size() > 1499) {
                    Trace.pop_front();
                }     
            }
    }
}

Point2D Neodometry::GetCoords() {
    Point2D loc;
    loc.x = x.load();
    loc.y = y.load();
    return loc;
}

int Neodometry::GetLeft() {
    return m_left_encoder.Get();
}
int Neodometry::GetRight() {
    return m_right_encoder.Get();
}

void Neodometry::RecordSwitch(bool arg) {
    m_recording.store(arg);
}


void Neodometry::DeleteTrace() {
    Trace.clear();
}

MotorOutputs Neodometry::Rewind() {
    auto current_time = std::chrono::high_resolution_clock::now();
    double time_step = std::chrono::duration<double>(current_time - lastRewindUpdate).count();
    int index = 0;
    MotorOutputs Outputs;
   
    double lastRewindDelta = std::chrono::duration<double>(lastRewindUse - current_time).count();

    Now.left_distance = m_left_encoder.GetDistance();
    Now.left_distance = m_right_encoder.GetDistance();

    if (Trace.size() > 0) {
        RecordFrame Target = Trace[Trace.size() -1];
        double currentLeftError = Target.left_distance - Now.left_distance;
        double currentRightError = Target.right_distance - Now.right_distance;

         if (!m_rewinding) {
            m_rewinding = true;
            m_recording.store(false);
            lastRewindUse = std::chrono::high_resolution_clock::now();
            firstOnLineTime = Target.timestamp;
         }   
        
        double targetTimeStamp = firstOnLineTime - lastRewindDelta;
        
        rightErrorIntegral += currentRightError * time_step;
        leftErrorIntegral += currentLeftError * time_step;

        double currentLeftErrorDelta = (currentLeftError - leftErrorLast) * time_step;
        double currentRightErrorDelta = (currentRightError - rightErrorLast) * time_step;
        double leftOutput = currentLeftError * kP + leftErrorIntegral * kI + currentLeftErrorDelta * kD;
        double rightOutput = currentRightError * kP + rightErrorIntegral * kI + currentRightErrorDelta * kD;

        if (Target.timestamp > targetTimeStamp) {
            Trace.pop_back();
        }
    } else {
        m_rewinding = false;
        m_recording.store(true);
    }

    lastRewindUpdate = std::chrono::high_resolution_clock::now();
    return Outputs;
}