#pragma once
#include <thread>
#include "Sensors/Deloder/Deloder.h"
#include <chrono>
#include <cmath>
#include <atomic>
#include <iostream>
#include "frc/Encoder.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include <iostream>
#include "Utilities/Datastructs/RecordFrame.h"
#include "Utilities/Datastructs/MotorOutputs.h"
#include <deque>
struct Point2D {
    double x =  0.0;
    double y =  0.0;
};


class Neodometry {
public:
   Neodometry(unsigned int dio1, unsigned int dio2, unsigned int dio3, unsigned int dio4);
   void NeodometryThread(std::atomic_bool& flag, std::atomic<double>& yaw);
   Point2D GetCoords();
   int GetLeft();
   int GetRight();
   void RecordSwitch(bool arg);
   MotorOutputs Rewind();
   void DeleteTrace();
private:
    std::atomic<double> x;
    std::atomic<double> y;
    frc::Encoder m_left_encoder;
    frc::Encoder m_right_encoder;
    double m_speed = 0.0;
    double right_speed = 0.0;
    double left_speed = 0.0;

    double xspeed_real = 0.0;
    double yspeed_real = 0.0;

    double xdelta = 0.0;
    double ydetla = 0.0;

    RecordFrame Frame;
    RecordFrame Now;
    RecordFrame Target;
    std::deque<RecordFrame> Trace;
    std::atomic<bool> m_recording;
    std::chrono::high_resolution_clock::time_point lastRewindUpdate;
    double leftErrorIntegral = 0.0;
    double rightErrorIntegral = 0.0;
    double leftErrorLast = 0.0;
    double rightErrorLast = 0.0;

    std::chrono::high_resolution_clock::time_point lastRewindUse;
    bool m_rewinding = false;

    double firstOnLineTime =  0.0;


    double kP = 0.0021;
    double kI = 0.0056;
    double kD = 023;
};
