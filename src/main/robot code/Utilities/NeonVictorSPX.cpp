#include "NeonVictorSPX.h"

NeonVictorSPX::NeonVictorSPX(unsigned int can_id) : VictorSPX(can_id), BaseMotorController(can_id | 0x1040000) {
    ConfigOpenloopRamp(1.0/4.0);
    SetNeutralMode(motorcontrol::Brake);
    ConfigVoltageCompSaturation(12,5);
    EnableVoltageCompensation(true);
}

void NeonVictorSPX::Power(float input) {
    if(input > 1.0) {
        input = 1.0;
    } else if(input < -1.0) {
        input = -1.0;
    }
    VictorSPX::Set(ControlMode::PercentOutput, input);
}
void NeonVictorSPX::DisableRamp() {
    VictorSPX::ConfigOpenloopRamp(0);
}
void NeonVictorSPX::OverWriteRamp(float set) {
    VictorSPX::ConfigOpenloopRamp(set);
}
void NeonVictorSPX::Brake() {
    VictorSPX::Set(ControlMode::PercentOutput, 0.0);
}






































// double NeonVictorSPX::GetOutput(){
// return ctx_vic.GetMotorOutputPercent();
// };
// void NeonVictorSPX::SetRamp(int sec_frac){
//     ctx_vic.ConfigOpenloopRamp(sec_frac);
// }
// void NeonVictorSPX::SetMotion(int speed){
//     ctx_vic.Set(ControlMode::PercentOutput, speed);
// }
// void NeonVictorSPX::Brake(){
//     ctx_vic.SetNeutralMode(motorcontrol::Brake);
// }